package com.company;

import java.util.Scanner;

public class InputOutput {
    Scanner scanner;

    public void exibirMensagem(String mensagem) {
        System.out.println(mensagem);
    }

    public InputOutput() {
        this.scanner = new Scanner(System.in);
    }

    public int validaFormatoGeomatrico() {
        System.out.println("Digite o Formato Geométrico de sua escolha: \n 1 - Circulo \n " +
                "3 - Triângulo \n 4 - Retângulo");

        return this.scanner.nextInt();
    }

    public double medidaCirculo(){
        System.out.println("Qual é o raio do Círculo: ");
        return scanner.nextDouble();
    }

    public double[] medidaRetangulo(){
        double[] medidaRetangulo = new double[2];
        System.out.println("Qual é a altura do Retangulo: ");
        medidaRetangulo[0] = scanner.nextDouble();
        System.out.println("Qual é a largura do Retangulo: ");
        medidaRetangulo[1] = scanner.nextDouble();
        return medidaRetangulo;
    }

    public double[] medidaTriangulo(){
        double[] medidaTriangulo = new double[3];
        System.out.println("Entre com o lado A: ");
        medidaTriangulo[0] = scanner.nextDouble();
        System.out.println("Entre com o lado B: ");
        medidaTriangulo[1] = scanner.nextDouble();
        System.out.println("Entre com o lado C: ");
        medidaTriangulo[2] = scanner.nextDouble();
        return medidaTriangulo;
    }

    public double retornaArea(Double area){

        System.out.println("A área do formato geométrico é de: "+area);
        return scanner.nextDouble();
    }

    public void trianguloInvalido(){
        System.out.println("Triangulo não é válido!!");
    }
}
